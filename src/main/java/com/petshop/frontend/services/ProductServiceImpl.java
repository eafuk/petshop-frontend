package com.petshop.frontend.services;

import com.petshop.frontend.models.Producto;
import com.petshop.frontend.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {
    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Iterable<Producto> listAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Producto getProductById(Integer id) {
        return productRepository.findOne(id);
    }

    @Override
    public Producto saveProduct(Producto product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(Integer id) {
        productRepository.delete(id);
    }

    @Override
    public Page<Producto> findAll(Pageable pageable) {
        return productRepository.findAll(pageable);
    }
}
