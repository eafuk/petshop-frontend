/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.frontend.services;

import com.petshop.frontend.models.Cita;
import com.petshop.frontend.models.Mascotas;
import com.petshop.frontend.repositories.MascotaRepository;
import com.petshop.frontend.repositories.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Antonio
 */
@Service
public class QuoteServiceImpl implements QuoteService {

	@Autowired
	private QuoteRepository quoteRepository;

	@Autowired
	private MascotaRepository mascotaRepository;

	@Override
	public void registrarCita(Cita quote) {

		Mascotas mascota = new Mascotas();

		mascota.setNombreMascota(quote.getMascota());
		mascota.setRazaId(quote.getRaza());
		mascota.setColorMascota(quote.getColor());
		mascota.setTamanoMascota(quote.getTamano());
		mascota.setDescripcionMascota(quote.getDescripcion());
		mascota = mascotaRepository.save(mascota);

		quote.setMascotaId(mascota.getId());
		quoteRepository.save(quote);
		quote.setHour_quote(null);
		quote.setRegister_date(null);
		quote.setMascota(null);
		quote.setTipo_mascota(null);
		quote.setColor(null);
		quote.setTamano(null);
		quote.setDescripcion(null);
	}

}
