package com.petshop.frontend.services;

import com.petshop.frontend.helpers.UrlHelper;
import com.petshop.frontend.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class EmailService {
    private JavaMailSender mailSender;

    @Autowired
    public EmailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Async
    public void sendEmail(SimpleMailMessage email) {
        mailSender.send(email);
    }

    public void sendRegistrationEmail(Usuario user, HttpServletRequest request)
    {
        String confirmationUrl = UrlHelper.create(request, "/confirm?token=" + user.getConfirmationToken());
        SimpleMailMessage mail = new SimpleMailMessage();

        mail.setTo(user.getEmail());
        mail.setSubject("PetShop - Confirmación de registro");
        mail.setText("Para confirmar tu cuenta, por favor ingresa al enlace de abajo:\n" + confirmationUrl);
        mail.setFrom("noreply@domain.com");
        this.sendEmail(mail);
    }
}
