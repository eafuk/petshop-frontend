package com.petshop.frontend.services;

import com.petshop.frontend.models.Cupon;
import com.petshop.frontend.repositories.CouponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {
    private CouponRepository couponRepository;

    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Override
    public List<Cupon> getAllCoupons() {
        return couponRepository.findAll();
    }

    @Override
    public Cupon getCouponById(int id) {
        return couponRepository.findOne(id);
    }

    @Override
    public Cupon getCouponByCode(String code) {
        return this.couponRepository.getByCode(code);
    }

    @Override
    public Cupon saveCoupon(Cupon coupon) {
        return couponRepository.save(coupon);
    }

    @Override
    public void deleteCoupon(Integer id) {
        couponRepository.delete(id);
    }

    @Override
    public Page<Cupon> findAll(Pageable pageable) {
        return couponRepository.findAll(pageable);
    }
}
