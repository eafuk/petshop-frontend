package com.petshop.frontend.services;

import com.petshop.frontend.models.Cart;
import com.petshop.frontend.models.CartLine;
import com.petshop.frontend.models.Cupon;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartService {
	@Autowired
	private HttpSession httpSession;
	private static final String CART_NAME = "petshop-cart";

	private Cart getFromSession() {
		Cart cart = (Cart)this.httpSession.getAttribute(CART_NAME);

		if (cart == null) {
			cart = new Cart();

			this.httpSession.setAttribute(CART_NAME, cart);
		}

		return cart;
	}

	private void updateInSession(Cart cart) {
		this.httpSession.setAttribute(CART_NAME, cart);
	}

	public CartLine add(CartLine line) {
		Cart cart = this.getFromSession();
		CartLine cartLine = cart.add(line);

		this.updateInSession(cart);

		return cartLine;
	}

	public void addCoupon(Cupon coupon) {
	    Cart cart = this.getFromSession();

	    cart.addCoupon(coupon);
	    this.updateInSession(cart);
    }

    public void removeCoupon(Cupon coupon) {
        Cart cart = this.getFromSession();

        cart.removeCoupon(coupon);
        this.updateInSession(cart);
    }

	public int getCount() {
		Cart cart = this.getFromSession();
		List<CartLine> lines = cart.getLines();
		int num = 0;

		if (lines != null) {
			for (CartLine line: lines) {
				num += line.getQuantity();
			}
		}

		return num;
	}

	public Cupon getCoupon() {
		Cart cart = this.getFromSession();

		return cart.getCoupon();
	}

	public List<CartLine> getLines() {
		Cart cart = this.getFromSession();

		return cart.getLines();
	}

	public Double getTotal() {
		Cart cart = this.getFromSession();
		List<CartLine> lines = cart.getLines();
		Double total = 0.0;

		if (lines != null) {
			for (CartLine line: lines) {
				Cupon coupon = cart.getCoupon();

				if (coupon != null) {
					total += line.getQuantity() * ((1.00 - (coupon.getDiscountPercentage() / 100.00)) * line.getProduct().getPrice());
				} else {
					total += line.getQuantity() * line.getProduct().getPrice();
				}
			}
		}

		return total;
	}

    public void remove(int productId) {
        Cart cart = this.getFromSession();

        cart.remove(productId);

        if (this.getCount() == 0) {
            Cupon coupon = this.getCoupon();

            if (coupon != null) {
                this.removeCoupon(coupon);
            }
        }

        this.updateInSession(cart);
    }

    public void update(List<CartLine> lines) {
        Cart cart = this.getFromSession();

        cart.update(lines);
        this.updateInSession(cart);
    }

    public void clear() {
        Cart cart = this.getFromSession();

        cart.clear();
        this.updateInSession(cart);
    }
}
