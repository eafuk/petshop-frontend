package com.petshop.frontend.services;

import com.petshop.frontend.models.LineaPedido;
import com.petshop.frontend.repositories.OrderLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderLineService {
    private OrderLineRepository orderLineRepository;

    @Autowired
    public OrderLineService(OrderLineRepository orderLineRepository) {
        this.orderLineRepository = orderLineRepository;
    }

    public void saveOrderLine(LineaPedido orderLine) {
        orderLineRepository.save(orderLine);
    }
}
