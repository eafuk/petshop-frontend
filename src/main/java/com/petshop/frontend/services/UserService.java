package com.petshop.frontend.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petshop.frontend.models.Cita;
import com.petshop.frontend.models.Horario;
import com.petshop.frontend.models.Raza;
import com.petshop.frontend.models.Rol;
import com.petshop.frontend.models.Servicios;
import com.petshop.frontend.models.Usuario;
import com.petshop.frontend.repositories.HorarioRepository;
import com.petshop.frontend.repositories.RazaRepository;
import com.petshop.frontend.repositories.ServicioRepository;
import com.petshop.frontend.repositories.UserRepository;

@Service
public class UserService {
	private RoleService roleService;
	private UserRepository userRepository;

	@Autowired
	private RazaRepository razaRepository;

	@Autowired
	private ServicioRepository servicioRepository;

	@Autowired
	private HorarioRepository horarioRepository;

	@Autowired
	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}

	public Usuario findByEmail(String email) {
		return userRepository.getByEmail(email);
	}

	public Usuario findByConfirmationToken(String confirmationToken) {
		return userRepository.getByConfirmationToken(confirmationToken);
	}

	public void saveUser(Usuario user) {
		Rol role = this.roleService.findByRole("USER");

		user.setRoles(new HashSet<>(Arrays.asList(role)));
		userRepository.save(user);
	}

	public List<Raza> obtenerRaza() {
		return razaRepository.findAll();
	}

	public List<Servicios> obtenerServicios() {
		return servicioRepository.findAll();
	}

	public List<Horario> obtenerHorario() {
		return horarioRepository.findAll();
	}

	public Cita obtenerUsuarioXQuote(Usuario user) {
		Cita quote = new Cita();
		user = userRepository.getByEmail(user.getEmail());

		quote.setDni(user.getDocumento());
		quote.setEmail(user.getEmail());
		quote.setName(user.getFirstName());
		quote.setLastname(user.getLastName());
		quote.setPhone(user.getPhone());

		return quote;
	}
}
