package com.petshop.frontend.services;

import com.petshop.frontend.models.Rol;
import com.petshop.frontend.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Rol findByRole(String role) {
        return roleRepository.findByRole(role);
    }

    public void saveRole(Rol role) {
        roleRepository.save(role);
    }
}
