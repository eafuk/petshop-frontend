package com.petshop.frontend.services;

import com.petshop.frontend.models.Cupon;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CouponService {
    List<Cupon> getAllCoupons();

    Cupon getCouponById(int id);

    Cupon getCouponByCode(String code);

    Cupon saveCoupon(Cupon coupon);

    void deleteCoupon(Integer id);

    Page<Cupon> findAll(Pageable pageable);
}
