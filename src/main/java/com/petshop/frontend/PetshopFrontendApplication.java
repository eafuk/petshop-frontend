package com.petshop.frontend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetshopFrontendApplication {
	public static void main(String[] args) {
		SpringApplication.run(PetshopFrontendApplication.class, args);
	}
}
