package com.petshop.frontend.repositories;

import com.petshop.frontend.models.Pedido;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<Pedido, Long> {
}
