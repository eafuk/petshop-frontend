/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.frontend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.petshop.frontend.models.Raza;

/**
 *
 * @author Antonio
 */
@Repository
public interface RazaRepository extends JpaRepository<Raza, Integer>{
    
}
