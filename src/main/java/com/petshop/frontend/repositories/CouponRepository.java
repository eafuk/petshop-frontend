package com.petshop.frontend.repositories;

import com.petshop.frontend.models.Cupon;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CouponRepository extends JpaRepository<Cupon, Integer> {
    Cupon getByCode(String code);

    Cupon getByUsuarioIdAndCode(int userId, String code);

    Cupon getByUsuarioIdAndCodeAndStatus(int userId, String code, boolean status);
}
