package com.petshop.frontend.repositories;

import com.petshop.frontend.models.Rol;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Rol, Long> {
    Rol findByRole(String role);
}
