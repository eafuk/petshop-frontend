package com.petshop.frontend.repositories;

import com.petshop.frontend.models.LineaPedido;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderLineRepository extends CrudRepository<LineaPedido, Long> {
}
