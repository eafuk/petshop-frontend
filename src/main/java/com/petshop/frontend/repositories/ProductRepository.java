package com.petshop.frontend.repositories;

import com.petshop.frontend.models.Producto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Producto, Integer> {
    Page<Producto> findAll(Pageable pageable);
}
