package com.petshop.frontend.controllers;

import com.petshop.frontend.models.*;
import com.petshop.frontend.repositories.CouponRepository;
import com.petshop.frontend.repositories.ProductRepository;
import com.petshop.frontend.repositories.UserRepository;
import com.petshop.frontend.services.*;
import com.petshop.frontend.utils.PageWrapper;
import com.petshop.frontend.utils.Response;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StoreController {
    private CartService cartService;
    private CouponRepository couponRepository;
    private CouponService couponService;
    private EmailService emailService;
    private OrderService orderService;
    private OrderLineService orderLineService;
    private ProductRepository productRepository;
    private ProductService productService;
    private UserRepository userRepository;
    private UserService userService;

    @Autowired
    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    @Autowired
    public void setCouponRepository(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Autowired
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }

    @Autowired
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @Autowired
    public void setOrderLineService(OrderLineService orderLineService) {
        this.orderLineService = orderLineService;
    }

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/cart", method = RequestMethod.GET)
    public ModelAndView showCart() {
        ModelAndView modelAndView = new ModelAndView();
        Cupon coupon = this.cartService.getCoupon();

        if (coupon != null) {
            modelAndView.addObject("coupon", coupon);
        }

        modelAndView.addObject("lines", this.cartService.getLines());
        modelAndView.addObject("total", this.cartService.getTotal());
        modelAndView.setViewName("carta");

        return modelAndView;
    }

    @RequestMapping(value = "/cart/add", method = RequestMethod.POST)
    @ResponseBody
    public Response add(@RequestParam("productId") String productId, @RequestParam("quantity") String quantity) {
        Producto product = this.productRepository.findOne(Integer.parseInt(productId));
        Integer qty = Integer.parseInt(quantity);

        this.cartService.add(new CartLine(product, qty));

        return new Response(true, this.cartService.getCount());
    }

    @RequestMapping(value = "/cart/add-coupon", method = RequestMethod.POST)
    @ResponseBody
    public Response addCoupon(@RequestParam("code") String code) {
        boolean status = false;
        String message = "";
        Double discountPercentage = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        
        if (!auth.getName().equals("anonymousUser")) {
            if (this.cartService.getCount() != 0) {
                String email = auth.getName();
                Usuario user = this.userRepository.getByEmail(email);
                Cupon coupon = this.couponRepository.getByUsuarioIdAndCode(user.getId(), code);

                if (coupon != null) {
                    if (coupon.getStatus() == Cupon.STATUS_UNUSED) {
                        this.cartService.addCoupon(coupon);

                        status = true;
                        message = "Ok";
                        discountPercentage = coupon.getDiscountPercentage();
                    } else {
                        message = "Error. El cupón ya fue usado";
                    }
                } else {
                    message = "Error. El cupón no existe";
                }
            } else {
                message = "Error. El carrito de compras está vacío";
            }
        } else {
            message = "Error. Debes iniciar sesión";
        }

        Map<String, Object> data = new HashMap<>();

        data.put("message", message);
        data.put("discountPercentage", discountPercentage);

        return new Response(status, data);
    }

    @RequestMapping(value = "/cart/remove-coupon", method = RequestMethod.POST)
    @ResponseBody
    public Response removeCoupon(@RequestParam("code") String code) {
        boolean status = false;
        String message = "";
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (!auth.getName().equals("anonymousUser")) {
            if (this.cartService.getCount() != 0) {
                String email = auth.getName();
                Usuario user = this.userRepository.getByEmail(email);
                Cupon coupon = this.couponRepository.getByUsuarioIdAndCode(user.getId(), code);

                if (coupon != null) {
                    this.cartService.removeCoupon(coupon);

                    status = true;
                    message = "Ok";
                } else {
                    message = "Error. El cupón no existe";
                }
            } else {
                message = "Error. El carrito de compras está vacío";
            }
        } else {
            message = "Error. Debes iniciar sesión";
        }

        Map<String, Object> data = new HashMap<>();

        data.put("message", message);

        return new Response(status, data);
    }
    
    @GetMapping(value = "/cart/buy")
    @ResponseBody
    public Response buyProduct() {
//        this.cartService.clear();

        Map<String, Object> data = new HashMap<>();

//        data.put("count", 0);
//        data.put("total", 0.0);
//        data.put("reload", true);

        return new Response(true, data);
    }

    @RequestMapping(value = "/cart/count", method = RequestMethod.GET)
    @ResponseBody
    public Response cartCount() {
        return new Response(true, this.cartService.getCount());
    }

    @RequestMapping(value = "/cart/remove", method = RequestMethod.POST)
    @ResponseBody
    public Response removeFromCart(@RequestParam("productId") String productId) {
        this.cartService.remove(Integer.parseInt(productId));

        int count = this.cartService.getCount();
        Double total = this.cartService.getTotal();
        Cupon coupon = this.cartService.getCoupon();
        boolean reload = (count == 0 && coupon != null);

        Map<String, Object> data = new HashMap<>();

        data.put("count", count);
        data.put("total", total);
        data.put("reload", reload);

        return new Response(true, data);
    }

    @RequestMapping(value = "/cart/update", method = RequestMethod.POST)
    @ResponseBody
    public Response updateQuantity(@RequestParam("productId") String productId, @RequestParam("quantity") String quantity) {
        Producto product = this.productRepository.findOne(Integer.parseInt(productId));
        Integer qty = Integer.parseInt(quantity);
        CartLine line = new CartLine(product, qty);

        this.cartService.update(Arrays.asList(line));

        Map<String, Object> data = new HashMap<>();
        Double subtotal = 0.00;
        Cupon coupon = this.cartService.getCoupon();

        if (coupon != null) {
            subtotal = qty * ((1.00 - (coupon.getDiscountPercentage() / 100.00)) * line.getProduct().getPrice());
        } else {
            subtotal = qty * line.getProduct().getPrice();
        }

        data.put("subtotal", subtotal);
        data.put("count", this.cartService.getCount());
        data.put("total", this.cartService.getTotal());

        return new Response(true, data);
    }

    @GetMapping("/checkout")
    public String showCheckoutForm(Model model) {
        model.addAttribute("order", new Pedido());

        return "checkout";
    }

    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public ModelAndView processOrder(@Valid @ModelAttribute("order") Pedido order, BindingResult bindingResult, HttpServletRequest request) {
        Usuario user = this.userRepository.getByEmail(order.getEmail());
        Cupon coupon = this.cartService.getCoupon();
        String message = "Gracias por comprar en nuestra tienda, su pedido estará llegando en un par de días.";
        boolean sendRegistrationEmail = false;

        // Process user
        if (order.getCreateAccount()) {
            if (user == null) {
                user = new Usuario();
                message += " No olvide revisar su bandeja de entrada para confirmar su cuenta de usuario.";

                user.setEmail(order.getEmail());
                user.setConfirmationToken(UUID.randomUUID().toString());

                sendRegistrationEmail = true;
            }

            user.setFirstName(order.getFirstName());
            user.setLastName(order.getLastName());
            user.setAddress(order.getAddress());
            user.setCity(order.getCity());
            user.setPostalCode(order.getPostalCode());
            user.setCountry(order.getCountry());
            user.setPhone(order.getPhone());

            this.userService.saveUser(user);
        }

        // Process order
        order.setUsuario(user);
        order.setTotal(this.cartService.getTotal());
        Pedido pedido = this.orderService.saveOrder(order);

        // Process order lines
        List<CartLine> cartLines = this.cartService.getLines();
        Double subtotal = 0.00;

        if (cartLines != null) {
            for (CartLine cartLine: cartLines) {
                LineaPedido orderLine = new LineaPedido();

                orderLine.setOrder(pedido);
                orderLine.setProductId(cartLine.getProduct().getId());
                orderLine.setQuantity(cartLine.getQuantity());

                if (coupon != null) {
                    subtotal = cartLine.getQuantity() * ((1.00 - (coupon.getDiscountPercentage() / 100.00)) * cartLine.getProduct().getPrice());
                } else {
                    subtotal = cartLine.getQuantity() * cartLine.getProduct().getPrice();
                }

                orderLine.setSubtotal(subtotal);

                this.orderLineService.saveOrderLine(orderLine);
            }
        }

        // Process coupon
        if (coupon != null) {
            coupon.setStatus(Cupon.STATUS_USED);

            this.couponRepository.save(coupon);
        }

        if (sendRegistrationEmail) {
            this.emailService.sendRegistrationEmail(user, request);
        }

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("message", message);
        modelAndView.setViewName("finalizar");

        return modelAndView;
    }

    @RequestMapping(value = "product/{id}", method = RequestMethod.GET)
    public String product(@PathVariable Integer id, Model model){
        model.addAttribute("product", productService.getProductById(id));

        return "producto";
    }

    @RequestMapping(value = "/store", method = RequestMethod.GET)
    public String showStore(Model model, Pageable pageable){
        Page<Producto> productPage = productService.findAll(pageable);
        PageWrapper<Producto> page = new PageWrapper<>(productPage, "/products");

        model.addAttribute("products", page.getContent());
        model.addAttribute("page", page);

        return "tienda";
    }
}
