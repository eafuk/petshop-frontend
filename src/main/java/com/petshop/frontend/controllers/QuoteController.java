/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.frontend.controllers;

import com.petshop.frontend.models.Cita;
import com.petshop.frontend.models.Horario;
import com.petshop.frontend.models.Raza;
import com.petshop.frontend.models.Servicios;
import com.petshop.frontend.models.Usuario;
import com.petshop.frontend.services.QuoteService;
import com.petshop.frontend.services.UserService;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Antonio
 */
@Controller
public class QuoteController {

    @Autowired
    private QuoteService quoteService;
    
    @Autowired
	private UserService userService;

    @RequestMapping(value = "/quote", method = RequestMethod.GET)
    public ModelAndView showQuotePage(ModelAndView modelAndView, Cita quote) {
        modelAndView.addObject("quote", quote);
        modelAndView.setViewName("cita");
        return modelAndView;
    }

    @RequestMapping(value = "/quote", method = RequestMethod.POST)
    public ModelAndView processRegistraterQuote(ModelAndView modelAndView, Cita quote, BindingResult bindingResult, HttpServletRequest request) {
        try {
            quote.setId(null);
            quoteService.registrarCita(quote);
            modelAndView.addObject("quote", quote);
            modelAndView.addObject("confirmationMessage", "Se ha registrado la cita correctamente");
            List<Horario> horarios = userService.obtenerHorario();
    		List<Raza> razas = userService.obtenerRaza();
    		List<Servicios> servicios = userService.obtenerServicios();
    		
    		modelAndView.addObject("isPay", true);
			modelAndView.addObject("isRegistro", false);
    		modelAndView.addObject("horario", horarios);
    		modelAndView.addObject("razas", razas);
    		modelAndView.addObject("servicios", servicios);
            modelAndView.setViewName("cita");
        } catch (Exception e) {
            modelAndView.addObject("errorRegisteredMessage", "Error al registrar cita");
            modelAndView.setViewName("cita");
        }

        return modelAndView;
    }
}
