package com.petshop.frontend.controllers;

import com.nulabinc.zxcvbn.Strength;
import com.nulabinc.zxcvbn.Zxcvbn;
import com.petshop.frontend.helpers.UrlHelper;
import com.petshop.frontend.models.Rol;
import com.petshop.frontend.models.Usuario;
import com.petshop.frontend.services.EmailService;
import com.petshop.frontend.services.UserService;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RegisterController {
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private UserService userService;
	private EmailService emailService;

	@Autowired
	public RegisterController(BCryptPasswordEncoder bCryptPasswordEncoder,
			UserService userService, EmailService emailService) {
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userService = userService;
		this.emailService = emailService;
	}

	@RequestMapping(value="/register", method = RequestMethod.GET)
	public ModelAndView showRegistrationPage(ModelAndView modelAndView, Usuario user) {
		modelAndView.addObject("user", user);
		modelAndView.setViewName("registro");
		return modelAndView;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView processRegistrationForm(ModelAndView modelAndView, @Valid Usuario user, BindingResult bindingResult, HttpServletRequest request) {
		Usuario userExists = userService.findByEmail(user.getEmail());

		if (userExists != null) {
			modelAndView.addObject("alreadyRegisteredMessage", "Lo sentimos. Ya existe una cuenta registrada con el mismo correo electrónico.");
			modelAndView.setViewName("registro");
			bindingResult.reject("email");
		}
                
                if (user.getGenero() == null) {
			modelAndView.addObject("alreadyRegisteredMessage", "El campo genero es obligatorio.");
			modelAndView.setViewName("registro");
			bindingResult.reject("genero");
		}

		if (bindingResult.hasErrors()) { 
                        modelAndView.addObject("user", user);
			modelAndView.setViewName("registro");		
		} else {
		    user.setStatus(false);
		    user.setConfirmationToken(UUID.randomUUID().toString());
		    userService.saveUser(user);

			String confirmationUrl = UrlHelper.create(request, "/confirm?token=" + user.getConfirmationToken());
			SimpleMailMessage registrationEmail = new SimpleMailMessage();

			registrationEmail.setTo(user.getEmail());
			registrationEmail.setSubject("PetShop - Confirmación de registro");
			registrationEmail.setText("Para confirmar tu cuenta, por favor ingresa al enlace de abajo:\n" + confirmationUrl);
			registrationEmail.setFrom("noreply@domain.com");
			emailService.sendEmail(registrationEmail);
                        modelAndView.addObject("user", new Usuario());
			modelAndView.addObject("confirmationMessage", "Se ha enviado un correo de confirmación a " + user.getEmail());
			modelAndView.setViewName("registro");
		}

		return modelAndView;
	}

	@RequestMapping(value="/confirm", method = RequestMethod.GET)
	public ModelAndView confirmRegistration(ModelAndView modelAndView, @RequestParam("token") String token) {
		Usuario user = userService.findByConfirmationToken(token);

		if (user == null) {
			modelAndView.addObject("invalidToken", "Lo sentimos. Este es un enlace de confirmación inválido.");
		} else {
			modelAndView.addObject("confirmationToken", user.getConfirmationToken());
		}

		modelAndView.setViewName("confirmar");

		return modelAndView;
	}

	@RequestMapping(value="/confirm", method = RequestMethod.POST)
	public ModelAndView confirmRegistration(ModelAndView modelAndView, BindingResult bindingResult, @RequestParam Map<String, String> requestParams, RedirectAttributes redir) {
		modelAndView.setViewName("confirmar");

		Zxcvbn passwordCheck = new Zxcvbn();
		Strength strength = passwordCheck.measure(requestParams.get("password"));

		if (strength.getScore() < 3) {
			bindingResult.reject("password");
			redir.addFlashAttribute("errorMessage", "Tu contraseña es muy débil. Escribe una más fuerte.");
			modelAndView.setViewName("redirect:confirm?token=" + requestParams.get("token"));
			System.out.println(requestParams.get("token"));

			return modelAndView;
		}

		Usuario user = userService.findByConfirmationToken(requestParams.get("token"));

		user.setPassword(bCryptPasswordEncoder.encode(requestParams.get("password")));
		user.setStatus(true);
		userService.saveUser(user);
		modelAndView.addObject("successMessage", "Tu contraseña ha sido establecida.");

		return modelAndView;
	}
}
