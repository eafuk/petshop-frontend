package com.petshop.frontend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.petshop.frontend.models.Cita;
import com.petshop.frontend.models.Horario;
import com.petshop.frontend.models.Raza;
import com.petshop.frontend.models.Servicios;
import com.petshop.frontend.models.Usuario;
import com.petshop.frontend.services.UserService;

@Controller
public class SiteController {

	@RequestMapping("/")
	public String index() {
		return "redirect:/home";
	}

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {
		ModelAndView modelAndView = new ModelAndView();

		modelAndView.setViewName("403");

		return modelAndView;
	}

	@GetMapping(value = "/citas")
	public ModelAndView citas() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = new Usuario();
		if (!auth.getName().equals("anonymousUser")) {
			String email = auth.getName();
			user.setEmail(email);
			Cita quote = userService.obtenerUsuarioXQuote(user);
			List<Horario> horarios = userService.obtenerHorario();
			List<Raza> razas = userService.obtenerRaza();
			List<Servicios> servicios = userService.obtenerServicios();
			user = userService.findByEmail(user.getEmail());

			modelAndView.addObject("isPay", true);
			modelAndView.addObject("isRegistro", false);
			modelAndView.addObject("user", user);
			modelAndView.addObject("horario", horarios);
			modelAndView.addObject("razas", razas);
			modelAndView.addObject("servicios", servicios);
			modelAndView.addObject("quote", quote);

			modelAndView.setViewName("cita");
		} else {
			modelAndView.addObject("message", "No se ha logeado");
			modelAndView.setViewName("index");
		}

		return modelAndView;
	}

	@GetMapping(value = "/cita/pay")
	public ModelAndView pagoCita() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario user = new Usuario();
		if (!auth.getName().equals("anonymousUser")) {
			String email = auth.getName();
			user.setEmail(email);
			Cita quote = userService.obtenerUsuarioXQuote(user);
			List<Horario> horarios = userService.obtenerHorario();
			List<Raza> razas = userService.obtenerRaza();
			List<Servicios> servicios = userService.obtenerServicios();
			user = userService.findByEmail(user.getEmail());

			modelAndView.addObject("isPay", false);
			modelAndView.addObject("isRegistro", true);
			modelAndView.addObject("user", user);
			modelAndView.addObject("horario", horarios);
			modelAndView.addObject("razas", razas);
			modelAndView.addObject("servicios", servicios);
			modelAndView.addObject("quote", quote);

			modelAndView.setViewName("cita");
		} else {
			modelAndView.addObject("message", "No se ha logeado");
			modelAndView.setViewName("index");
		}

		return modelAndView;
	}
}
