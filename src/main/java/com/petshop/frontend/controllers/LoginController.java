package com.petshop.frontend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.petshop.frontend.models.Cita;
import com.petshop.frontend.models.Horario;
import com.petshop.frontend.models.Producto;
import com.petshop.frontend.models.Raza;
import com.petshop.frontend.models.Servicios;
import com.petshop.frontend.models.Usuario;
import com.petshop.frontend.services.UserService;
import com.petshop.frontend.utils.PageWrapper;

@Controller
public class LoginController {

	private Facebook facebook;
	private ConnectionRepository connectionRepository;

	@Autowired
	private UserService userService;

	@GetMapping(value = "/home")
	public ModelAndView defaultPage(ModelAndView modelAndView) {

		modelAndView.setViewName("index");
		return modelAndView;
	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public ModelAndView showLoginPage(ModelAndView modelAndView, Usuario user) {
		modelAndView.addObject("user", user);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (!auth.getName().equals("anonymousUser")) {
			String email = auth.getName();
			user.setEmail(email);
			Cita quote = userService.obtenerUsuarioXQuote(user);
			List<Horario> horarios = userService.obtenerHorario();
			List<Raza> razas = userService.obtenerRaza();
			List<Servicios> servicios = userService.obtenerServicios();
			user = userService.findByEmail(user.getEmail());

			modelAndView.addObject("isPay", true);
			modelAndView.addObject("isRegistro", false);
			modelAndView.addObject("user", user);
			modelAndView.addObject("horario", horarios);
			modelAndView.addObject("razas", razas);
			modelAndView.addObject("servicios", servicios);
			modelAndView.addObject("quote", quote);

			modelAndView.setViewName("cita");
		} else {
			modelAndView.setViewName("logueo");
		}
		return modelAndView;
	}
	
	@GetMapping(value = "/loginPost")
	public String defaultLogin(Model model, Pageable pageable) {
//		PageWrapper<Producto> page = new PageWrapper<>(null, "/home");
		return "redirect:/home";
	}

	@PostMapping(value = { "/loginPost" })
	public ModelAndView loginPage(ModelAndView modelAndView, Usuario user) {
		Cita quote = userService.obtenerUsuarioXQuote(user);
		List<Horario> horarios = userService.obtenerHorario();
		List<Raza> razas = userService.obtenerRaza();
		List<Servicios> servicios = userService.obtenerServicios();
		user = userService.findByEmail(user.getEmail());

		UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(user.getEmail(),
				user.getPassword());

		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authReq);

		modelAndView.addObject("user", user);
		modelAndView.addObject("horario", horarios);
		modelAndView.addObject("razas", razas);
		modelAndView.addObject("servicios", servicios);
		modelAndView.addObject("quote", quote);

		modelAndView.setViewName("index");
		return modelAndView;
	}
	
	

	public ModelAndView userInfo(ModelAndView modelAndView, Usuario user) {

		user = userService.findByEmail(user.getEmail());

		modelAndView.addObject("user", user);

		return modelAndView;
	}

	@GetMapping(value = "/connect/facebook")
	@ResponseBody
	public User helloFacebook(ModelAndView model) {
		if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
//			model.setViewName("logueo");
			return new User(null, null, null, null, null, null);
		}

		String[] fields = { "id", "about", "age_range", "birthday", "context", "cover", "currency", "devices",
				"education", "email", "favorite_athletes", "favorite_teams", "first_name", "gender", "hometown",
				"inspirational_people", "installed", "install_type", "is_verified", "languages", "last_name", "link",
				"locale", "location", "meeting_for", "middle_name", "name", "name_format", "political", "quotes",
				"payment_pricepoints", "relationship_status", "religion", "security_settings", "significant_other",
				"sports", "test_group", "timezone", "third_party_id", "updated_time", "verified", "video_upload_limits",
				"viewer_can_send_gift", "website", "work" };

		User userProfile = facebook.fetchObject("me", User.class, fields);

		model.addObject("facebookProfile", facebook.userOperations().getUserProfile());
//		model.addAttribute("facebookProfile", userProfile);
		return userProfile;
	}
}
