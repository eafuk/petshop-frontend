package com.petshop.frontend.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "linea_pedido")
public class LineaPedido {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_pedido", nullable = false)
    private Pedido pedido;

    @Column(name = "id_producto")
    @NotNull(message = "Este campo es requerido")
    private int productId;

    @Column(name = "cantidad")
    @NotNull(message = "Este campo es requerido")
    private int quantity;

    @Column(name = "subtotal", columnDefinition = "DECIMAL(10, 2)")
    @NotNull(message = "Este campo es requerido")
    private Double subtotal;

    @Version
    private Integer version = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pedido getOrder() {
        return pedido;
    }

    public void setOrder(Pedido pedido) {
        this.pedido = pedido;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
