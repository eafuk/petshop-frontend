package com.petshop.frontend.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class Cart implements Serializable {
    private static final long serialVersionUID = 4573229359755965961L;
    private Cupon coupon = null;
    private LinkedHashMap<Integer, CartLine> map = new LinkedHashMap<>();

    public CartLine add(CartLine cartLine) {
        if (this.map.containsKey(cartLine.getProduct().getId())) {
            CartLine existingCartLine = this.map.get(cartLine.getProduct().getId());
            int newQuantity = existingCartLine.getQuantity() + cartLine.getQuantity();
            CartLine newCartLine = new CartLine(cartLine.getProduct(), newQuantity);

            this.map.put(cartLine.getProduct().getId(), newCartLine);

            return newCartLine;
        } else {
            this.map.put(cartLine.getProduct().getId(), cartLine);

            return cartLine;
        }
    }

    public void addCoupon(Cupon coupon) {
        if (this.coupon == null) {
            this.coupon = coupon;
        }
    }

    public void removeCoupon(Cupon coupon) {
        if (this.coupon.getCode().equals(coupon.getCode())) {
            this.coupon = null;
        }
    }

    public Cupon getCoupon() {
        return this.coupon;
    }

    public void remove(int productId) {
        this.map.remove(productId);
    }

    public void update(List<CartLine> cartLines) {
        if (cartLines != null) {
            for (CartLine cartLine : cartLines) {
                update(cartLine);
            }
        }
    }

    private void update(CartLine cartLine) {
        if (this.map.containsKey(cartLine.getProduct().getId())) {
            if (cartLine.getQuantity() <= 0) {
                remove(cartLine.getProduct().getId());
            } else {
                map.put(cartLine.getProduct().getId(), cartLine);
            }
        } else {
            map.put(cartLine.getProduct().getId(), cartLine);
        }
    }

    public List<CartLine> getLines() {
        return new ArrayList<CartLine>(this.map.values());
    }

    public void clear() {
        this.map.clear();
    }
}
