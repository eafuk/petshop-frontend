package com.petshop.frontend.models;

import java.io.Serializable;

public class CartLine implements Serializable {
	private static final long serialVersionUID = -3480296374500403880L;
	private int quantity;
	private Producto product;

	public CartLine() {

	}
	
	public CartLine(Producto product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Producto getProduct() {
		return product;
	}

	public void setProduct(Producto product) {
		this.product = product;
	}
}
