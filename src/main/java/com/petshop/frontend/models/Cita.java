/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.frontend.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Antonio  
 */

@Entity
@Table(name = "cita")
public class Cita {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "nombres")
    private String name;
    
    @Column(name = "apellidos")
    private String lastname;
    
    @Column(name = "dni")
    private String dni;
    
    @Column(name = "correo")
    private String email;
    
    @Column(name = "telefono")
    private String phone;
    
    @Column(name = "tipo")
    private String type;
    
    @Column(name = "fecha_registro")
    private String register_date;
    
    @Column(name = "hora_cita")
    private String hour_quote;
    
    @Column(name = "comentarios")
    private String comments;
    
    @Column(name = "mascota")
    private String mascota;
    
    @Column(name = "tipo_mascota")
    private Integer tipo_mascota;
    
    @Column(name = "mascota_id")
    private Integer mascotaId;
    
    @Column(name = "horario_id")
    private Integer horarioId;
    
    @Column(name = "servicio_id")
    private Integer servicioId;
    
    @Transient
    private String color;
    
    @Transient
    private Integer raza;
    
    @Transient
    private String tamano;
    
    @Transient
    private String descripcion;
    
    @Transient
    private Integer servicio;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the dni
     */
    public String getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the register_date
     */
    public String getRegister_date() {
        return register_date;
    }

    /**
     * @param register_date the register_date to set
     */
    public void setRegister_date(String register_date) {
        this.register_date = register_date;
    }

    /**
     * @return the hour_quote
     */
    public String getHour_quote() {
        return hour_quote;
    }

    /**
     * @param hour_quote the hour_quote to set
     */
    public void setHour_quote(String hour_quote) {
        this.hour_quote = hour_quote;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the mascota
     */
    public String getMascota() {
        return mascota;
    }

    /**
     * @param mascota the mascota to set
     */
    public void setMascota(String mascota) {
        this.mascota = mascota;
    }

    /**
     * @return the tipo_mascota
     */
    public Integer getTipo_mascota() {
        return tipo_mascota;
    }

    /**
     * @param tipo_mascota the tipo_mascota to set
     */
    public void setTipo_mascota(Integer tipo_mascota) {
        this.tipo_mascota = tipo_mascota;
    }

    /**
     * @return the mascotaId
     */
    public Integer getMascotaId() {
        return mascotaId;
    }

    /**
     * @param mascotaId the mascotaId to set
     */
    public void setMascotaId(Integer mascotaId) {
        this.mascotaId = mascotaId;
    }

    /**
     * @return the horarioId
     */
    public Integer getHorarioId() {
        return horarioId;
    }

    /**
     * @param horarioId the horarioId to set
     */
    public void setHorarioId(Integer horarioId) {
        this.horarioId = horarioId;
    }

    /**
     * @return the servicioId
     */
    public Integer getServicioId() {
        return servicioId;
    }

    /**
     * @param servicioId the servicioId to set
     */
    public void setServicioId(Integer servicioId) {
        this.servicioId = servicioId;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * @return the raza
     */
    public Integer getRaza() {
        return raza;
    }

    /**
     * @param raza the raza to set
     */
    public void setRaza(Integer raza) {
        this.raza = raza;
    }

    /**
     * @return the tamano
     */
    public String getTamano() {
        return tamano;
    }

    /**
     * @param tamano the tamano to set
     */
    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the servicio
     */
    public Integer getServicio() {
        return servicio;
    }

    /**
     * @param servicio the servicio to set
     */
    public void setServicio(Integer servicio) {
        this.servicio = servicio;
    }
    

    
}
