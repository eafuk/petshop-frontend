package com.petshop.frontend.models;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "cupon")
public class Cupon {
    public static final boolean STATUS_UNUSED = false;
    public static final boolean STATUS_USED = true;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    @Column(name = "codigo", length = 8)
    @NotEmpty(message = "Este campo es requerido")
    private String code;

    @Column(name = "porcentaje_descuento", columnDefinition = "DECIMAL(10, 2)")
    @NotNull(message = "Este campo es requerido")
    private Double discountPercentage = 1.00;

    @Column(name = "estatus")
    @NotNull(message = "Este campo es requerido")
    private boolean status = STATUS_UNUSED;

    @Version
    private Integer version = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(Double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public boolean getStatus() {
        return status;
    }

    public Map getStatuses() {
        Map<Boolean, String> statuses = new HashMap<>();

        statuses.put(STATUS_UNUSED, "No usado");
        statuses.put(STATUS_USED, "Usado");

        return statuses;
    }

    public String getStatusName() {
        Map statuses = this.getStatuses();

        return statuses.get(this.status) != null ? (String)statuses.get(this.status) : "";
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
