package com.petshop.frontend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Transient;

@Entity
@Table(name = "usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "correo", nullable = false, unique = true)
    @Email(message = "Ingrese un correo electrónico válido")
    @NotEmpty(message = "Este campo es requerido")
    private String email;

    @Transient
    @Column(name = "contrasena")
    private String password;

    @Column(name = "token_confirmacion")
    private String confirmationToken;

    @Column(name = "nombres", length = 128)
    @NotEmpty(message = "Este campo es requerido")
    private String firstName;

    @Column(name = "apellidos", length = 128)
    @NotEmpty(message = "Este campo es requerido")
    private String lastName;

    @Column(name = "direccion")
    private String address;

    @Column(name = "ciudad")
    private String city;

    @Column(name = "codigo_postal")
    private String postalCode;

    @Column(name = "pais")
    private String country;

    @Column(name = "telefono")
    private String phone;

    @Column(name = "estatus")
    private boolean status;

    @Version
    private Integer version = 0;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "rol_id"))
    @JsonIgnore
    private Set<Rol> roles;

    @OneToMany(mappedBy = "usuario")
    private Set<Cupon> cupones;
    
    @Column(name = "tipo_doc")
    private Integer tipoDoc;
    
    @Column(name = "fecha_nac")
    private Date fechaNac;
    
    @Column(name = "celular")
    private String celular;
    
    @Column(name = "genero")
    private Integer genero;
    
    @Column(name = "documento")
    private String documento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmationToken() {
        return confirmationToken;
    }

    public void setConfirmationToken(String confirmationToken) {
        this.confirmationToken = confirmationToken;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean value) {
        this.status = value;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Set<Rol> getRoles() {
        return roles;
    }

    public void setRoles(Set<Rol> roles) {
        this.roles = roles;
    }

    /**
     * @return the cupones
     */
    public Set<Cupon> getCupones() {
        return cupones;
    }

    /**
     * @param cupones the cupones to set
     */
    public void setCupones(Set<Cupon> cupones) {
        this.cupones = cupones;
    }

    /**
     * @return the tipoDoc
     */
    public Integer getTipoDoc() {
        return tipoDoc;
    }

    /**
     * @param tipoDoc the tipoDoc to set
     */
    public void setTipoDoc(Integer tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    
    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

  
    public String getCelular() {
        return celular;
    }

    
    public void setCelular(String celular) {
        this.celular = celular;
    }

    
    public Integer getGenero() {
        return genero;
    }

   
    public void setGenero(Integer genero) {
        this.genero = genero;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    
    
}
