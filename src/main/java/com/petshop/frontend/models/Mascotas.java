/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.petshop.frontend.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mascotas")
public class Mascotas {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "raza_id")
    private Integer razaId;
    
    @Column(name = "nombre_mascota")
    private String nombreMascota;
    
    @Column(name = "tipo_mascota")
    private String tipoMascota;
    
    @Column(name = "color_mascota")
    private String colorMascota;
    
    @Column(name = "tamano_mascota")
    private String tamanoMascota;
    
    @Column(name = "descripcion_mascota")
    private String descripcionMascota;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the razaId
     */
    public Integer getRazaId() {
        return razaId;
    }

    /**
     * @param razaId the razaId to set
     */
    public void setRazaId(Integer razaId) {
        this.razaId = razaId;
    }

    /**
     * @return the nombreMascota
     */
    public String getNombreMascota() {
        return nombreMascota;
    }

    /**
     * @param nombreMascota the nombreMascota to set
     */
    public void setNombreMascota(String nombreMascota) {
        this.nombreMascota = nombreMascota;
    }

    /**
     * @return the tipoMascota
     */
    public String getTipoMascota() {
        return tipoMascota;
    }

    /**
     * @param tipoMascota the tipoMascota to set
     */
    public void setTipoMascota(String tipoMascota) {
        this.tipoMascota = tipoMascota;
    }

    /**
     * @return the colorMascota
     */
    public String getColorMascota() {
        return colorMascota;
    }

    /**
     * @param colorMascota the colorMascota to set
     */
    public void setColorMascota(String colorMascota) {
        this.colorMascota = colorMascota;
    }

    /**
     * @return the tamanoMascota
     */
    public String getTamanoMascota() {
        return tamanoMascota;
    }

    /**
     * @param tamanoMascota the tamanoMascota to set
     */
    public void setTamanoMascota(String tamanoMascota) {
        this.tamanoMascota = tamanoMascota;
    }

    /**
     * @return the descripcionMascota
     */
    public String getDescripcionMascota() {
        return descripcionMascota;
    }

    /**
     * @param descripcionMascota the descripcionMascota to set
     */
    public void setDescripcionMascota(String descripcionMascota) {
        this.descripcionMascota = descripcionMascota;
    }
    
    
    
}
