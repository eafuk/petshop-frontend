package com.petshop.frontend.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "`pedido`")
public class Pedido {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JsonIgnore
    private Usuario usuario;

    @Column(name = "correo")
    @Email(message = "Ingrese un correo electrónico válido")
    @NotEmpty(message = "Este campo es requerido")
    private String email;

    @Column(name = "nombres")
    @NotEmpty(message = "Este campo es requerido")
    private String firstName;

    @Column(name = "apellidos")
    @NotEmpty(message = "Este campo es requerido")
    private String lastName;

    @Column(name = "direccion")
    @NotEmpty(message = "Este campo es requerido")
    private String address;

    @Column(name = "ciudad")
    @NotEmpty(message = "Este campo es requerido")
    private String city;

    @Column(name = "codigo_postal")
    @NotEmpty(message = "Este campo es requerido")
    private String postalCode;

    @Column(name = "pais")
    @NotEmpty(message = "Este campo es requerido")
    private String country;

    @Column(name = "telefono")
    @NotEmpty(message = "Este campo es requerido")
    private String phone;

    @Column(name = "notas", columnDefinition = "TEXT")
    private String notes;

    @Column(name = "total", columnDefinition = "DECIMAL(10, 2)")
    @NotNull(message = "Este campo es requerido")
    private Double total;

    @Version
    private Integer version = 0;

    @Transient
    private boolean createAccount = false;

    @OneToMany(mappedBy = "pedido")
    @JsonIgnore
    private Set<LineaPedido> pedidos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public boolean getCreateAccount() {
        return createAccount;
    }

    public void setCreateAccount(boolean createAccount) {
        this.createAccount = createAccount;
    }

    /**
     * @return the pedidos
     */
    public Set<LineaPedido> getPedidos() {
        return pedidos;
    }

    /**
     * @param pedidos the pedidos to set
     */
    public void setPedidos(Set<LineaPedido> pedidos) {
        this.pedidos = pedidos;
    }
}
