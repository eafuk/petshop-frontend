package com.petshop.frontend.services;

import com.petshop.frontend.models.Producto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
    Iterable<Producto> listAllProducts();

    Producto getProductById(Integer id);

    Producto saveProduct(Producto product);

    void deleteProduct(Integer id);

    Page<Producto> findAll(Pageable pageable);
}
