package com.petshop.frontend.services;

import com.petshop.frontend.models.Rol;
import com.petshop.frontend.models.Usuario;
import com.petshop.frontend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

@Service
public class UserService {
    private RoleService roleService;
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    public Usuario findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public Usuario findByConfirmationToken(String confirmationToken) {
        return userRepository.findByConfirmationToken(confirmationToken);
    }

    public void saveUser(Usuario user) {
        Rol role = this.roleService.findByRole("USER");

        user.setRoles(new HashSet<>(Arrays.asList(role)));
        userRepository.save(user);
    }
}
