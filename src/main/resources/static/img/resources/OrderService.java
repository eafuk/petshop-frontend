package com.petshop.frontend.services;

import com.petshop.frontend.models.Pedido;
import com.petshop.frontend.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    private OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public void saveOrder(Pedido order) {
        orderRepository.save(order);
    }
}
