$(document).ready(function () {
    $('.add-to-cart').click(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'post',
            url: '/cart/add',
            data: {
                _csrf: $('input[name="_csrf"]').val(),
                productId: $(this).data('pk'),
                quantity: document.getElementById('quantity').value
            },
            success: function (response) {
                if (response.status) {
                    $('#cart-count').text(response.data);
                } else {

                }

                console.log(response);
            },
            error: function (e) {
                // alert("Error");
                console.log('Error: ', e);
            }
        });
    });
});
