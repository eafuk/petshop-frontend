"use strict";
//1. Jquery Tabs Box
if ($('.tabs-box').length) {
    //Tabs
    $('.tabs-box .tab-buttons .tab-btn').click(function (e) {

        e.preventDefault();
        var target = $($(this).attr('href'));

        target.parents('.tabs-box').children('.tab-buttons').children('.tab-btn').removeClass('active-btn');
        $(this).addClass('active-btn');
        target.parents('.tabs-box').children('.tab-content').children('.tab').fadeOut(0);
        target.parents('.tabs-box').children('.tab-content').children('.tab').removeClass('active-tab');
        $(target).fadeIn(300);
        $(target).addClass('active-tab');
    });

}
//2.  Main menu
function mainmenu() {
    //Submenu Dropdown Toggle
    if ($('.main-menu li.dropdown ul').length) {
        $('.main-menu li.dropdown').append('<div class="dropdown-btn"></div>');

        //Dropdown Button
        $('.main-menu li.dropdown .dropdown-btn').click(function () {
            $(this).prev('ul').slideToggle(500);
        });
    }

}

//3. Hide Loading Box (Preloader)
function handlePreloader() {
    if ($('.preloader').length) {
        $('.preloader').delay(200).fadeOut(500);
    }
}
//4. Scroll to a Specific Div
if ($('.scroll-to-target').length) {
    $(".scroll-to-target").click(function () {
        var target = $(this).attr('data-target');
        // animate
        $('html, body').animate({
            scrollTop: $(target).offset().top
        }, 1000);

    });
}
//5. Update Scroll to Top
function headerStyle() {
    if ($('.main-header').length) {
        var windowpos = $(window).scrollTop();
        if (windowpos >= 200) {
            $('.main-header').addClass('fixed-header');
            $('.scroll-to-top').fadeIn(300);
        } else {
            $('.main-header').removeClass('fixed-header');
            $('.scroll-to-top').fadeOut(300);
        }
    }
}

headerStyle();

//6. Elements Animation
if ($('.wow').length) {
    var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 0, // distance to the element when triggering the animation (default is 0)
        mobile: false, // trigger animations on mobile devices (default is true)
        live: true // act on asynchronously loaded content (default is true)
    });
    wow.init();
}
// Dom Ready Function
jQuery(document).ready(function () {
    (function ($) {
        // add your functions
        mainmenu();
    })(jQuery);
});
// instance of fuction while Window Load event
jQuery(window).load(function () {
    (function ($) {
        handlePreloader()

    })(jQuery);
});

(function ($) {

    $(window).scroll(function () {
        headerStyle();
    });

})(window.jQuery);
