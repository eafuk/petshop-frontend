$(document).ready(function () {
    $('.remove-from-cart').click(function (e) {
        e.preventDefault();

        var primaryKey = $(this).data('pk');

        $.ajax({
            type: 'post',
            url: '/cart/remove',
            data: {
                _csrf: $('input[name="_csrf"]').val(),
                productId: primaryKey
            },
            success: function (response) {
                if (response.status) {
                    $('#cart-count').text(response.data.count);
                    $('#cart-total').text(response.data.total);
                    $('#tr-' + primaryKey).remove();

                    if (response.data.reload) {
                        window.location.reload(true);
                    }
                }

                console.log(response);
            },
            error: function (e) {
                // alert("Error");
                console.log('Error: ', e);
            }
        });
    });

    $('.quantity').click(function (e) {
        $('#cart-items').block();

        var primaryKey = $(this).data('pk');
        
        console.log(primaryKey);

        $.ajax({
            type: 'post',
            url: '/cart/update',
            data: {
                _csrf: $('input[name="_csrf"]').val(),
                productId: primaryKey,
                quantity: $(this).val()
            },
            success: function (response) {
                if (response.status) {
                    $('#subtotal-' + primaryKey).text(response.data.subtotal);
                    $('#cart-count').text(response.data.count);
                    $('#cart-total').text(response.data.total);
                }

                console.log(response);
                $('#cart-items').unblock();
            },
            error: function (e) {
                // alert("Error");
                console.log('Error: ', e);
            }
        });
    });

    $('#add-coupon').click(function () {
        var url = $(this).data('url');
        var data = {
            _csrf: $('input[name="_csrf"]').val(),
            code: $('#coupon-code').val()
        };

        $.post(url, data, function (response) {
            if (response.status) {
                window.location.reload(true);
            } else {
                swal({
                    icon: 'error',
                    title: 'PetShop',
                    text: response.data.message
                });
            }
        });
    });

    $('#remove-coupon').click(function () {
        var url = $(this).data('url');
        var data = {
            _csrf: $('input[name="_csrf"]').val(),
            code: $('#coupon-code').val()
        };

        $.post(url, data, function (response) {
            if (response.status) {
                window.location.reload(true);
            } else {
                swal({
                    icon: 'error',
                    title: 'PetShop',
                    text: response.data.message
                });
            }
        });
    });
});
