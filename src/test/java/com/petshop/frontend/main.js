$(document).ready(function () {
    $.ajax({
        type: 'get',
        url: '/cart/count',
        data: {},
        success: function (response) {
            if (response.status) {
                $('#cart-count').text(response.data);
            } else {

            }

            console.log(response);
        },
        error: function (e) {
            console.log('Error: ', e);
        }
    });
});
